﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FuturaMod1.Models;

// You cannot carry anymore.

namespace FuturaMod1.Controllers
{
    public class PlacesController : Controller
    {
        private PlacesEntities db = new PlacesEntities();
        // static string to store postcode
        private static string thePost = "";

        // GET: Start Page
        public ActionResult Start()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Start(string postCode)
        {
            // bind the input of postcode from cshtml
            thePost = postCode;
            return RedirectToAction("Index");
        }

        // GET: Start Page
        public ActionResult StartError()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StartError(string postCode)
        {
            // bind the input of postcode from cshtml
            thePost = postCode;
            return RedirectToAction("Index");
        }

        // GET: Places
        public ActionResult Index(string postString, string searchString, bool communityCheck = false, bool centreCheck = false, bool clubCheck = false, bool parkCheck = false)
        {
            // use viewbag to display postcode and search name in view
            //thePost = postString;
            ViewBag.ShowPost = thePost;
            ViewBag.ShowName = searchString;

            // to store the 4 checkbox status and error when nothing is there
            ViewBag.def1 = true;
            ViewBag.def2 = true;
            ViewBag.def3 = true;
            ViewBag.def4 = true;
            if (communityCheck == false && centreCheck == false && clubCheck == false && parkCheck == false)
            {
                ViewBag.def1 = false;
                ViewBag.def2 = false;
                ViewBag.def3 = false;
                ViewBag.def4 = false;
            }
            ViewBag.chk1 = true;
            ViewBag.chk2 = true;
            ViewBag.chk3 = true;
            ViewBag.chk4 = true;
            if (communityCheck == false)
                ViewBag.chk1 = false;
            if (centreCheck == false)
                ViewBag.chk2 = false;
            if (clubCheck == false)
                ViewBag.chk3 = false;
            if (parkCheck == false)
                ViewBag.chk4 = false;

            // check if user changed the postcode
            string getPost = postString;
            var places = from p in db.Places
                         where p.Postcode == thePost
                         select p;
            if (getPost != null) // if the page is postback
            {
                ViewBag.ShowPost = getPost;
                thePost = getPost;
                places = from p in db.Places
                         where p.Postcode == getPost
                         select p;
            }

            // not finding anything, return to start page, display error
                if (!places.Any())
            {
                return RedirectToAction("StartError");
            }

            // handles manual search function
            if (!String.IsNullOrEmpty(searchString))
            {
                places = places.Where(p => p.Name.Contains(searchString));
            }

            // if nothing is selected just return all
            if (communityCheck == false && centreCheck == false && clubCheck == false && parkCheck == false)
            {
                return View(places.ToList());
            }

            // creates an empty var combine, add each selected element to it
            var placesComm = places.Where(p => p.PlaceType.Equals("Community"));
            var placesCent = places.Where(p => p.PlaceType.Equals("Aged Care Centre"));
            var placesClub = places.Where(p => p.PlaceType.Equals("Club"));
            var placesPark = places.Where(p => p.PlaceType.Equals("Park"));
            var combine = places.Where(p => p.PlaceType.Equals("nothing"));
            if (communityCheck == true)
                combine = combine.Concat(placesComm);
            if (centreCheck == true)
                combine = combine.Concat(placesCent);
            if (clubCheck == true)
                combine = combine.Concat(placesClub);
            if (parkCheck == true)
                combine = combine.Concat(placesPark);

            // display error when nothing is there
            ViewBag.Err = "";
            if (!combine.Any())
            {
                ViewBag.Err = "Oops, nothing there, try again :(";
            }

            // find the places with matched postcode
                return View(combine.ToList());
        }

        // GET: Places/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Places places = db.Places.Find(id);
            if (places == null)
            {
                return HttpNotFound();
            }
            return View(places);
        }

        //// GET: Places/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Places/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Name,Address,Suburb,State,Postcode,CareType,Provider,OrgType,Remote,Latitude,Longitude,PlaceType,Desc")] Places places)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Places.Add(places);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(places);
        //}

        //// GET: Places/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Places places = db.Places.Find(id);
        //    if (places == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(places);
        //}

        //// POST: Places/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,Name,Address,Suburb,State,Postcode,CareType,Provider,OrgType,Remote,Latitude,Longitude,PlaceType,Desc")] Places places)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(places).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(places);
        //}

        //// GET: Places/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Places places = db.Places.Find(id);
        //    if (places == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(places);
        //}

        //// POST: Places/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Places places = db.Places.Find(id);
        //    db.Places.Remove(places);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
